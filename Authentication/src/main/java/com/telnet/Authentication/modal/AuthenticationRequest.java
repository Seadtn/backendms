package com.telnet.Authentication.modal;


import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthenticationRequest {
	private Integer id;
	private String username;
	private boolean isEnabled;
	@Enumerated(EnumType.STRING)
	private UserRole role;
}
