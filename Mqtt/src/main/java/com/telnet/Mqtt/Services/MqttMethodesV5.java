package com.telnet.Mqtt.Services;

import org.eclipse.paho.mqttv5.client.MqttClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.telnet.Mqtt.modal.ConnectedClientv5;

import usp.UspMsg13.Header.MsgType;

@Service
public interface MqttMethodesV5 {

	public void publish(byte[] msg, String topic, String respTopic, boolean retain, MqttClient mc, int qos)
			throws JsonProcessingException;

	public ByteString waitForResponse(String topic, MqttClient mqttClient, int qos, byte[] tr369Message,Integer idUser,MsgType type,String RequestString)
			throws MqttException, InterruptedException, JsonProcessingException, InvalidProtocolBufferException;

	public ConnectedClientv5 GetConnectedClient(String idClient);
	public void RemoveClient(String idClient) throws MqttException;

}
