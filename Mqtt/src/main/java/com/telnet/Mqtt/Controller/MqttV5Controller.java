package com.telnet.Mqtt.Controller;


import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import com.google.gson.JsonObject;
import com.google.protobuf.ByteString;
import com.telnet.Mqtt.Services.GlobalMethods;
import com.telnet.Mqtt.Services.MqttMethodesV5;
import com.telnet.Mqtt.modal.USPRequest;

import usp.UspMsg13.Header.MsgType;
import usp.UspMsg13.Msg;

@RestController
@RequestMapping("/mqtt/v5")
public class MqttV5Controller {
	@Autowired
	private MqttMethodesV5 mqttMethodes;
	@Autowired
	private GlobalMethods globalMethods;



	@PostMapping("/getRequest/{id}/parameters")
	public ResponseEntity<?> sendGetMsg(@PathVariable("id") String idClient, @RequestBody String paramPathsJson,
			@RequestParam("maxDepth") String maxDepth,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if (!json) {
			JsonObject pramaPathObject = new Gson().fromJson(paramPathsJson, JsonObject.class);
			String paramPaths = pramaPathObject.get("paramPaths").getAsJsonArray().toString();
			Parameters.put("paramPaths", paramPaths);
			Parameters.put("maxDepth", maxDepth);
		} else {
			JsonObject pramaPathObject = new Gson().fromJson(paramPathsJson, JsonObject.class);
	        String paramPathsJsonString = pramaPathObject.get("paramPaths").getAsString();
			Parameters.put("json", paramPathsJsonString);
		}
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.GET, idClient, Parameters, json);

		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.GET,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}	
		return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));

	}

	@PostMapping("/getInstanceRequest/{id}/parameters")
	public ResponseEntity<?> sendGetInstanceMsg(@PathVariable("id") String idClient, @RequestBody String objPathsJson,
			@RequestParam("firstLevelOnly") String firstLevelOnly,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if(!json) {
		JsonObject objPathsObject = new Gson().fromJson(objPathsJson, JsonObject.class);
		String objPaths = objPathsObject.get("paramPaths").getAsString();
		Parameters.put("objPaths", objPaths);
		Parameters.put("firstLevelOnly", firstLevelOnly);
		}else {
			JsonObject pramaPathObject = new Gson().fromJson(objPathsJson, JsonObject.class);
	        String objPathsJsonString = pramaPathObject.get("paramPaths").getAsString();
			Parameters.put("json", objPathsJsonString);
		}
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.GET_INSTANCES, idClient, Parameters, json);
		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.GET_INSTANCES,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));

	}

	@PostMapping("/getSupportedDM/{id}/parameters")
	public ResponseEntity<?> sendSupportedDM(@PathVariable("id") String idClient, @RequestBody String objPathsJson,
			@RequestParam("firstLevelOnly") String firstLevelOnly,
			@RequestParam("returnCommands") String returnCommands, @RequestParam("returnEvents") String returnEvents,
			@RequestParam("returnParams") String returnParams,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if(!json) {
		JsonObject objPathsObject = new Gson().fromJson(objPathsJson, JsonObject.class);
		String objPaths = objPathsObject.get("objPaths").getAsString();
		Parameters.put("objPaths", objPaths);
		Parameters.put("firstLevelOnly", firstLevelOnly);
		Parameters.put("returnCommands", returnCommands);
		Parameters.put("returnEvents", returnEvents);
		Parameters.put("returnParams", returnParams);
		}else {
			JsonObject pramaPathObject = new Gson().fromJson(objPathsJson, JsonObject.class);
	        String objPathsJsonString = pramaPathObject.get("objPaths").getAsString();
			Parameters.put("json", objPathsJsonString);
		}
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.GET_SUPPORTED_DM, idClient, Parameters, json);
		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.GET_SUPPORTED_DM,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));
	}

	@GetMapping("/getSupportedProto/{id}/parameters")
	public ResponseEntity<?> sendSupportedProto(@PathVariable("id") String idClient,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.GET_SUPPORTED_PROTO, idClient, Parameters, json);

		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.GET_SUPPORTED_PROTO,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));
	}

	@PostMapping("/Set/{id}/parameters")
	public ResponseEntity<?> sendSetMsg(@PathVariable("id") String idClient,
			@RequestParam("allowPartial") String allowPartial, @RequestBody String paramSettings,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if(!json) {
		Parameters.put("allowPartial", allowPartial);
		Parameters.put("paramSettings", paramSettings);
		}else {
			JsonObject pramaPathObject = new Gson().fromJson(paramSettings, JsonObject.class);
	        String objPathsJsonString = pramaPathObject.get("Params").getAsString();
			Parameters.put("json", objPathsJsonString);
		}
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.SET, idClient, Parameters, json);

		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.SET,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));
	}

	@PostMapping("/Add/{id}/parameters")
	public ResponseEntity<?> sendAddMsg(@PathVariable("id") String idClient,
			@RequestParam("allowPartial") String allowPartial, @RequestBody String paramSettings,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if(!json){
		Parameters.put("allowPartial", allowPartial);
		Parameters.put("paramSettings", paramSettings);
		}else {
			JsonObject pramaPathObject = new Gson().fromJson(paramSettings, JsonObject.class);
	        String objPathsJsonString = pramaPathObject.get("Params").getAsString();
			Parameters.put("json", objPathsJsonString);
		}
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.ADD, idClient, Parameters, json);

		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.ADD,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));
	}

	@PostMapping("/Operate/{id}/parameters")
	public ResponseEntity<?> sendOperateMsg(@PathVariable("id") String idClient,@RequestBody String inputArgs,
			@RequestParam("command") String command, @RequestParam("commandKey") String commandKey,
			@RequestParam("sendResp") String sendResp,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if(!json) {
		Parameters.put("command", command);
		Parameters.put("commandKey", commandKey);
		Parameters.put("inputArgs", inputArgs);
		Parameters.put("sendResp", sendResp);
		}else {
			JsonObject pramaPathObject = new Gson().fromJson(inputArgs, JsonObject.class);
	        String commandJsonString = pramaPathObject.get("input").getAsString();
			Parameters.put("json", commandJsonString);
		}
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.OPERATE, idClient, Parameters, json);
		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.OPERATE,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));

	}

	@PostMapping("/Delete/{id}/parameters")
	public ResponseEntity<?> sendDeleteMsg(@PathVariable("id") String idClient,
			@RequestParam("allowPartial") String allowPartial, @RequestBody String objPathsJson,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json,
			@RequestParam("idUser") Integer idUser) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if(!json) {
		JsonObject objPathsObject = new Gson().fromJson(objPathsJson, JsonObject.class);
		String objPaths = objPathsObject.get("objPaths").getAsString();
		Parameters.put("allowPartial", allowPartial);
		Parameters.put("objPaths", objPaths);
		}else {
			JsonObject pramaPathObject = new Gson().fromJson(objPathsJson, JsonObject.class);
	        String objPathsJsonString = pramaPathObject.get("objPaths").getAsString();
			Parameters.put("json", objPathsJsonString);
		}
		USPRequest request= globalMethods.CreateUSPRecord(MsgType.DELETE, idClient, Parameters, json);

		ByteString responseData = mqttMethodes.waitForResponse("reply/" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				request.getRequestByte(),idUser,MsgType.DELETE,request.getRequestString());
		if(responseData.toStringUtf8().contains("No Response")) {
			return  ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(responseData.toStringUtf8());

		}
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(globalMethods.checkIfHasError(Msg.parseFrom(responseData))));

	}

}