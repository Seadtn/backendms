package com.telnet.Mqtt.modal;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Errors")
public class Error {
	@Id
	private String errorCode;
	private String errorMsg;
	@Transient
	private String requestedPath;
	@Transient
	private String expectedErrorCode;
}
