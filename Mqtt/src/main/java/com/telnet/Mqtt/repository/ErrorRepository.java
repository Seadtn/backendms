package com.telnet.Mqtt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telnet.Mqtt.modal.Error;
@Repository
public interface ErrorRepository  extends JpaRepository<Error, String>{
	Error findByErrorCode(String errorCode);
	Error findByErrorMsg(String errorMsg);

}
