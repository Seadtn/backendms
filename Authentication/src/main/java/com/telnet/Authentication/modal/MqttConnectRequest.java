package com.telnet.Authentication.modal;




import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter @Setter
@ToString
public class MqttConnectRequest {


	private String idBoard;
    private String uspVersions;
    private String endpointId;
    private String protocol;
	private String brokerUrl;
    private String brokerUserName;
    private String brokerPassword;
    private String controllerId;

    private MQTTVersion mqttVersion;
    private int qos;
    private boolean tls;
    

}
