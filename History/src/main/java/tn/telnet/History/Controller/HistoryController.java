package tn.telnet.History.Controller;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

import lombok.RequiredArgsConstructor;
import tn.telnet.History.Repository.HistoryRepository;
import tn.telnet.History.modal.History;

@Controller
@RequiredArgsConstructor
@RequestMapping("/api/history")
public class HistoryController {
	private final HistoryRepository historyRepository;
	
	@PostMapping("/save")
	public ResponseEntity<String> saveNewRequest(@RequestBody String requestJson){
			History history = new Gson().fromJson(requestJson, History.class);
			historyRepository.save(history);
			return ResponseEntity.ok("saved");
	}
	
	@GetMapping("/getHistoryByIdClient")
	public ResponseEntity<?> getHistoryByIdClient(@RequestParam("idClient") Integer id){
		return ResponseEntity.ok(new Gson().toJson(historyRepository.findByIdClient(id)));
	}
	
	@GetMapping("/getHistoryByIdBoard")
	public ResponseEntity<?> getHistoryByIdBoard(@RequestParam("idBoard") String id){
        return ResponseEntity.ok(new Gson().toJson(historyRepository.findByIdBoard(id)));
	}
	
	@GetMapping("/Delete")
	public void delete(@RequestParam(name = "id" ) Integer id) {
		historyRepository.deleteById(id);
	}
	@GetMapping("/DeleteByIdBoard")
	public void deleteByIdBoard(@RequestParam(name = "id" ) String id) {
		historyRepository.deleteByIdBoard(id);
	}
	
}
