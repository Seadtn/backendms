package com.telnet.Mqtt.modal;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultResponse {
    String result;
    List<Error> errors;
    List<Error> expectedErrors;
    List<Error> unknownErrors;
}
