package com.telnet.Mqtt.Controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.protobuf.ByteString;
import com.telnet.Mqtt.Services.GlobalMethods;
import com.telnet.Mqtt.Services.MqttMethodesV3;

import usp.UspMsg13.Header.MsgType;
import usp.UspMsg13.Msg;
@RestController
@RequestMapping({ "/mqtt/v3", "/mqtt/v3_1_1" })
public class MqttV3Controller {
	@Autowired
	private MqttMethodesV3 mqttMethodes;
	@Autowired
	private GlobalMethods globalMethods;
	@PostMapping("/getRequest/{id}/parameters")
	public ResponseEntity<?> sendGetMsg(@PathVariable("id") String idClient, @RequestBody String paramPathsJson,
			@RequestParam("maxDepth") String maxDepth,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		if (!json) {
			JsonObject pramaPathObject = new Gson().fromJson(paramPathsJson, JsonObject.class);
			String paramPaths = pramaPathObject.get("paramPaths").getAsString();
			Parameters.put("paramPaths", paramPaths);
			Parameters.put("maxDepth", maxDepth);
		} else {
			Parameters.put("json", paramPathsJson);
		}
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.GET, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());

	}

	@PostMapping("/getInstanceRequest/{id}/parameters")
	public ResponseEntity<?> sendGetInstanceMsg(@PathVariable("id") String idClient, @RequestBody String objPathsJson,
			@RequestParam("firstLevelOnly") String firstLevelOnly,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		JsonObject objPathsObject = new Gson().fromJson(objPathsJson, JsonObject.class);
		String objPaths = objPathsObject.get("paramPaths").getAsString();
		Parameters.put("objPaths", objPaths);
		Parameters.put("firstLevelOnly", firstLevelOnly);
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.GET_INSTANCES, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());

	}

	@PostMapping("/getSupportedDM/{id}/parameters")
	public ResponseEntity<?> sendSupportedDM(@PathVariable("id") String idClient, @RequestBody String objPathsJson,
			@RequestParam("firstLevelOnly") String firstLevelOnly,
			@RequestParam("returnCommands") String returnCommands, @RequestParam("returnEvents") String returnEvents,
			@RequestParam("returnParams") String returnParams,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		JsonObject objPathsObject = new Gson().fromJson(objPathsJson, JsonObject.class);
		String objPaths = objPathsObject.get("objPaths").getAsString();
		Parameters.put("objPaths", objPaths);
		Parameters.put("firstLevelOnly", firstLevelOnly);
		Parameters.put("returnCommands", returnCommands);
		Parameters.put("returnEvents", returnEvents);
		Parameters.put("returnParams", returnParams);
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.GET_SUPPORTED_DM, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());
	}

	@GetMapping("/getSupportedProto/{id}/parameters")
	public ResponseEntity<?> sendSupportedProto(@PathVariable("id") String idClient,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.GET_SUPPORTED_PROTO, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());
	}

	@PostMapping("/Set/{id}/parameters")
	public ResponseEntity<?> sendSetMsg(@PathVariable("id") String idClient,
			@RequestParam("allowPartial") String allowPartial, @RequestBody String paramSettings,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		Parameters.put("allowPartial", allowPartial);
		Parameters.put("paramSettings", paramSettings);
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.SET, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());
	}

	@PostMapping("/Add/{id}/parameters")
	public ResponseEntity<?> sendAddMsg(@PathVariable("id") String idClient,
			@RequestParam("allowPartial") String allowPartial, @RequestBody String paramSettings,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		Parameters.put("allowPartial", allowPartial);
		Parameters.put("paramSettings", paramSettings);
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.ADD, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());
	}

	@PostMapping("/Operate/{id}/parameters")
	public ResponseEntity<?> sendOperateMsg(@PathVariable("id") String idClient,
			@RequestParam("command") String command, @RequestParam("commandKey") String commandKey,
			@RequestParam("sendResp") String sendResp,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		Parameters.put("command", command);
		Parameters.put("commandKey", commandKey);
		Parameters.put("sendResp", sendResp);
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.OPERATE, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());

	}

	@DeleteMapping("/Delete/{id}/parameters")
	public ResponseEntity<?> sendDeleteMsg(@PathVariable("id") String idClient,
			@RequestParam("allowPartial") String allowPartial, @RequestBody String objPathsJson,
			@RequestParam(name = "json", required = false, defaultValue = "false") boolean json) throws Exception {
		HashMap<String, String> Parameters = new HashMap<String, String>();
		JsonObject objPathsObject = new Gson().fromJson(objPathsJson, JsonObject.class);
		String objPaths = objPathsObject.get("objPaths").getAsString();
		Parameters.put("allowPartial", allowPartial);
		Parameters.put("objPaths", objPaths);
		ByteString responseData = mqttMethodes.waitForResponse("reply%2F" + idClient,
				mqttMethodes.GetConnectedClient(idClient).getMqttClient(), 0,
				globalMethods.CreateUSPRecord(MsgType.DELETE, idClient, Parameters, json).getRequestByte());
		return ResponseEntity.status(HttpStatus.OK).body(Msg.parseFrom(responseData).toString());

	}
}
