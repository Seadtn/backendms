package com.telnet.Notification.WsConfig.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
@Service
public class WebSocketService {
	private final SimpMessagingTemplate messagingTemplate;
	
	@Autowired
	public WebSocketService(SimpMessagingTemplate messagingTemplate) {
		this.messagingTemplate=messagingTemplate;
	}
	
	public void notifyUser(String id,String topic,ResponseEntity<?> responseEntity) {
		messagingTemplate.convertAndSendToUser(id, topic, responseEntity);
	}
}
