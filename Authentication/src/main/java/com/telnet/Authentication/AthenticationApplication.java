package com.telnet.Authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AthenticationApplication.class, args);
	}

}
