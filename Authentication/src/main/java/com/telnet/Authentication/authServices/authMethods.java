package com.telnet.Authentication.authServices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.telnet.Authentication.modal.Account;
import com.telnet.Authentication.modal.MQTTVersion;
import com.telnet.Authentication.modal.MqttAgent;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface authMethods {


    public String generateRandomPassword(int len);


    public ByteString CheckMQTTRegister(String version, String id, String controllerId,Integer idUser)
            throws MqttSecurityException, MqttException, JsonProcessingException, InterruptedException, Exception;

    public ResponseEntity<String> CheckRegisterProtocol(String protocol, String version, String id,
                                                        String controllerId, Account account) throws JsonProcessingException, MqttException, InterruptedException,
            InvalidProtocolBufferException, Exception;

    public ResponseEntity<String> SaveNewMqttAgent(String id, String version, String endpointID,
                                                   String password, String protocol , String controllerId, MQTTVersion mqttversion,Account account)
            throws JsonProcessingException, MqttException, InterruptedException, org.eclipse.paho.mqttv5.common.MqttException;

    public ResponseEntity<String> CheckMQTTConnection(MqttAgent mqttAgent,Account account)
            throws JsonProcessingException, MqttException, InterruptedException,
            org.eclipse.paho.client.mqttv3.MqttException, org.eclipse.paho.mqttv5.common.MqttException;

    public ResponseEntity<String> CheckConnectionProtocol(String idBoard,Account account)
            throws JsonProcessingException, MqttException, InterruptedException,
            org.eclipse.paho.client.mqttv3.MqttException, org.eclipse.paho.mqttv5.common.MqttException;

    public ResponseEntity<String> CheckDisconnectionProtocol(String protocol,String version , String idclient,String username)
            throws JsonProcessingException, MqttException, InterruptedException,
            org.eclipse.paho.client.mqttv3.MqttException, org.eclipse.paho.mqttv5.common.MqttException;

    public void DisconnectMQTTAgent(String idClient,String version,String username) throws JsonProcessingException, org.eclipse.paho.mqttv5.common.MqttException,
            InterruptedException, org.eclipse.paho.client.mqttv3.MqttException;

    public void CheckMqttParameters(MqttAgent mqttAgent, String brokerUrl,String username, String password,
                                    boolean tls, int qos);
}
