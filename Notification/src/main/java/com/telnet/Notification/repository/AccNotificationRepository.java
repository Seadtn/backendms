package com.telnet.Notification.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.telnet.Notification.modal.AccNotification;
import com.telnet.Notification.modal.AccNotification.AccType;

public interface AccNotificationRepository  extends JpaRepository<AccNotification, Integer> {
	List<AccNotification> findByIdsenderOrIdreceiver(Integer ids,Integer idr);
	List<AccNotification>findByNotificationType(AccType accType);
}
