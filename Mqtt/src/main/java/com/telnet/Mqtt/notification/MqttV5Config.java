package com.telnet.Mqtt.notification;

import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttCallback;
import org.eclipse.paho.mqttv5.client.MqttClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import com.google.protobuf.InvalidProtocolBufferException;
import com.telnet.Mqtt.Services.GlobalMethods;
import com.telnet.Mqtt.modal.Notification;

import usp.UspMsg13.Msg;
import usp.UspMsg13.Notify;
import usp_record.UspRecord13.Record;

@Configuration
public class MqttV5Config {

    @Autowired
    private KafkaTemplate<String, Notification> kafkaTemplate;
    @Autowired
    private GlobalMethods globalMethods;
	@Value("${mqtt.server-uri}")
	private String brokerUrl;
	@Value("${mqtt.username}")
	private String username;
	@Value("${mqtt.password}")
	private String password;

	@Bean
	public MqttClient mqttClient(@Autowired MqttConnectionOptions mqttConnectOptions) throws MqttException {
	    MqttClient client = new MqttClient(brokerUrl, "UspController", new MemoryPersistence());
	    
	    client.setCallback(new MqttCallback() {
	        @Override
	        public void messageArrived(String topic, MqttMessage message) throws Exception {
	            String inputTopic = "notify/";
	            if (topic.startsWith(inputTopic)) {
	            	Notify notify = null;
	            	String[] splitArray = topic.split("/");
	            	String idBoard = splitArray[splitArray.length - 1];
	            	
	        		try {
	        			Record record = Record.parseFrom(message.getPayload());
	        			Msg msg = Msg.parseFrom(record.getNoSessionContext().getPayload());
	        			notify=msg.getBody().getRequest().getNotify();
	        		} catch (InvalidProtocolBufferException e) {
	        			e.printStackTrace();
	        		}
	        		Notification notification=globalMethods.checkNotificationType(notify, idBoard);
	                kafkaTemplate.send("mqtt-notification", notification);            }
	        }

			@Override
			public void disconnected(MqttDisconnectResponse disconnectResponse) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mqttErrorOccurred(MqttException exception) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void deliveryComplete(IMqttToken token) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void connectComplete(boolean reconnect, String serverURI) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void authPacketArrived(int reasonCode, MqttProperties properties) {
				// TODO Auto-generated method stub
				
			}
	    });
	    
	    client.connect(mqttConnectOptions);
	    client.subscribe("#", 0);
	    
	    return client;
	}

	@Bean
	public MqttConnectionOptions mqttConnectOptions() {
	    MqttConnectionOptions options = new MqttConnectionOptions();
	    options.setServerURIs(new String[] { brokerUrl });
	    options.setUserName(username);
	    options.setPassword(password.getBytes()); 
	    options.setCleanStart(true);
	    return options;
	}
}