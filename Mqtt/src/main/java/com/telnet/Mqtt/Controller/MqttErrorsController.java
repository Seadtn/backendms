package com.telnet.Mqtt.Controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.telnet.Mqtt.modal.Error;
import com.telnet.Mqtt.repository.ErrorRepository;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/mqtt/errors")
@RequiredArgsConstructor
public class MqttErrorsController {
	private final ErrorRepository errorRepository;
	@GetMapping("/GetAll")
	public ResponseEntity<?> getAllErrors(){
		List<Error> errors = errorRepository.findAll();
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(errors));
	}
	@PostMapping("/addError")
	public void addError(@RequestParam("errorCode") String errorCode , @RequestParam("errorMsg") String errorMsg ){
		errorRepository.save(new Error(errorCode,errorMsg,null,null));
	}
}
