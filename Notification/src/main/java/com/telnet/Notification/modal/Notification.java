package com.telnet.Notification.modal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String idBoard;
	@Enumerated(EnumType.STRING)
	private NotificationType notificationType; 
	@Column(columnDefinition = "LONGTEXT") 
	private String value;
	private String time;
    public enum NotificationType {
        Event,
        ValueChange,
        ObjectDeletion,
        ObjectCreation,
        OperationComplete,
        OnBoardRequest;
    }
	
}
