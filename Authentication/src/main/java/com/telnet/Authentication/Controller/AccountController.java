package com.telnet.Authentication.Controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.telnet.Authentication.modal.Account;
import com.telnet.Authentication.modal.Agent;
import com.telnet.Authentication.modal.AuthenticationRequest;
import com.telnet.Authentication.modal.Status;
import com.telnet.Authentication.modal.UserRole;
import com.telnet.Authentication.repository.AccountRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@RestController
@RequestMapping("/auth/account")
@RequiredArgsConstructor
@Slf4j
public class AccountController {
	private final AccountRepository accountRepository;
	private final PasswordEncoder passwordEncoder;
	public static HashMap<Integer, Integer> accounts = new HashMap<>();
    private final  KafkaTemplate<String, String> kafkaTemplate;

	@GetMapping("/getAllUsers")
	public ResponseEntity<?> getAllUsers(){
		log.info("Getting all user........");
		List<Account> accounts = accountRepository.findAll();
		accounts.forEach(t -> t.setListAgents(null));
		log.info("User List : "+accounts.toString());
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(accounts));
	}
	
	@PostMapping("/Connect")
	public ResponseEntity<?> connect(@RequestBody String connectRequestJson) {
		JsonObject connectRequest = new Gson().fromJson(connectRequestJson, JsonObject.class);
		String username = connectRequest.get("name").getAsString();
		String password = connectRequest.get("pswd").getAsString();
		boolean account = accountRepository.findByUsername(username).isPresent();
		Account acc =null;
		if (!account ) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User doesnt exist !!");
		}else {
			 acc = accountRepository.findByUsername(username).get();
			if(!passwordEncoder.matches(password, acc.getPassword())) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Incorrect password  !!");
			}
		}
		if(!acc.isEnabled()) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Your account is locked");
		}
		if(acc.getStatus().equals(Status.Offline)) {
		 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		 	Date date = new Date();  
		 	acc.setLastLogin(formatter.format(date));
		 	acc.setStatus(Status.Online);
			accountRepository.save(acc);
		}
		if(accounts.containsKey(acc.getId())) {
			accounts.put(acc.getId(), accounts.get(acc.getId())+1);
		}else {
			accounts.put(acc.getId(), 1);
		}
		AuthenticationRequest authenticationRequest = new AuthenticationRequest(acc.getId(),acc.getUsername(),acc.isEnabled(),acc.getRole());
		log.info("Connected User : "+new Gson().toJson(authenticationRequest));
		return ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(authenticationRequest));
	}
	
	@GetMapping("/Disconnect/{id}")
	public void disconnect(@PathVariable Integer id) {
		if(accounts.get(id)==1) {
			accounts.remove(id);
			Account account = accountRepository.findById(id).get();
			log.info("User is disconnected  : "+account.getUsername());
			account.setStatus(Status.Offline);
			accountRepository.save(account);
		}else {
			accounts.put(id, accounts.get(id)-1);
		}
	}
	
	@PostMapping("/Register")
	public ResponseEntity<?> register(@RequestBody String registerRequestJson) {
		JsonObject connectRequest = new Gson().fromJson(registerRequestJson, JsonObject.class);
		String username = connectRequest.get("name").getAsString();
		String password = connectRequest.get("pswd").getAsString();
		boolean account = accountRepository.findByUsername(username).isPresent();
		if (!account) {
		 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		 	Date date = new Date();  
			Account acc = new Account(null,username,passwordEncoder.encode(password),Status.Offline, UserRole.SubAdmin,new ArrayList<Agent>(),true,"User has not connected yet",formatter.format(date));
			accountRepository.save(acc);
			log.info("New User is added : "+username);
			return ResponseEntity.status(HttpStatus.CREATED).body("User Created Succesfully !");
		}else {
			return ResponseEntity.status(HttpStatus.FOUND).body("Username Exist ! \n");
		}
	}
	
	@PostMapping("/Modify")
	public ResponseEntity<?> modify(@RequestBody String modifyRequestJson) {
		JsonObject connectRequest = new Gson().fromJson(modifyRequestJson, JsonObject.class);
		Integer id = connectRequest.get("id").getAsInt();
		String username = connectRequest.get("username").getAsString();
		String password = connectRequest.get("pswd").getAsString();
		Account account = accountRepository.findById(id).get();
		account.setUsername(username);
		if(!password.isEmpty() && !password.equals(account.getPassword())) {
		account.setPassword(passwordEncoder.encode(password));}
		accountRepository.save(account);
		log.info("User "+account.getUsername()+" is Modified");
		return ResponseEntity.status(HttpStatus.OK).body("User "+account.getUsername()+" Modified");
	}
	@GetMapping("/ChangeState")
	public void changeState(@RequestParam(name = "id" ) Integer id) {
			Account account = accountRepository.findById(id).get();
			account.setEnabled(!account.isEnabled());
			kafkaTemplate.send("account-lock", id+"");
			accountRepository.save(account);	
	}
	
	@GetMapping("/Delete")
	public void delete(@RequestParam(name = "id" ) Integer id) {
		log.info("User "+id+" is Deleted");
		accountRepository.deleteById(id);
	}
	

}
