package com.telnet.Authentication.repository;

import com.telnet.Authentication.modal.Agent;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentRepository  extends JpaRepository<Agent,String> {
	List<Agent> findByAccountId(Integer id);
}
