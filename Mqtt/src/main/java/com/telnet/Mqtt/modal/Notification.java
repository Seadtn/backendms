package com.telnet.Mqtt.modal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



import com.fasterxml.jackson.annotation.JsonProperty;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
	
    @JsonProperty("id")
    private Integer id;
    
    @JsonProperty("idBoard")
    private String idBoard;
    
    @JsonProperty("notificationType")
    private NotificationType notificationType;
    
    @JsonProperty("value")
    private String value;
    
    @JsonProperty("time")
    private String time;
    

    
    public enum NotificationType {
        Event,
        ValueChange,
        ObjectDeletion,
        ObjectCreation,
        OperationComplete,
        OnBoardRequest;
    }
}
