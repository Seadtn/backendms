package com.telnet.Notification.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.telnet.Notification.WsConfig.service.WebSocketService;
import com.telnet.Notification.kafka.listener.KafkaListeners;
import com.telnet.Notification.modal.AccNotification;
import com.telnet.Notification.modal.WebSession;
import com.telnet.Notification.repository.AccNotificationRepository;
import com.telnet.Notification.repository.NotificationRepository;

import lombok.RequiredArgsConstructor;

import com.telnet.Notification.modal.AccNotification.AccType;

@Controller
@RequestMapping("/api/notification")
@RequiredArgsConstructor
public class NotifyController {
	public static List<AccNotification> permissionQueue= new ArrayList<>();

	private final WebSocketService service;
	private final AccNotificationRepository accNotificationRepository;
	private final NotificationRepository notificationRepository;
	@PostMapping("/SendPermission")
	public ResponseEntity<String> sendPermission(@RequestParam("idSender") Integer idSender,@RequestParam("idReciver") Integer idReciver,@RequestParam("username") String username,@RequestParam("idBoard") String boardId,@RequestParam("receiverName") String receiverName) {
		boolean notif = permissionQueue.stream()
			    .filter(notify -> (notify.getIdsender().equals(idSender) && notify.getIdreceiver().equals(idReciver)) || notify.getIdsender().equals(idSender))
			    .findFirst().isPresent();
		
		if(notif) {
			return ResponseEntity.ok("Denied: You are connecting to another board. Please terminate the existing connection.");	
		}
		
		boolean notifi = permissionQueue.stream()
			    .filter(notify -> notify.getIdreceiver().equals(idReciver))
			    .findFirst().isPresent();
		
		if(notifi) {
			return ResponseEntity.ok("The client you're attempting to connect to is currently in another session. Please try again later.");	
		}
		
		WebSession s = null;
		for(WebSession session : KafkaListeners.webSessions) {
			if(session.getUserId().contains(",")) {
				if(session.getUserId().split(",")[0].equals(idReciver.toString())) {
					s=session;
					break;
				}
			}else if(session.getUserId().equals(idReciver.toString())){
				s=session;
				break;
			}
			
		}
	 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	 	Date date = new Date();  
		AccNotification accNotification = new AccNotification(null,idSender,idReciver,AccType.permission,"Request to connect from user  "+username +" to the board with id  "+boardId,formatter.format(date),username,receiverName);
		accNotificationRepository.save(accNotification);
		if(s!=null) {
			permissionQueue.add(accNotification);
			service.notifyUser(s.getSessionId(), "/topic/notification", ResponseEntity.ok(accNotification));
			return  ResponseEntity.ok("Sending.... request to connect");
		}
		return ResponseEntity.ok("Accepted") ;	
	}
	
	@PostMapping("/deletePermission")
	public ResponseEntity<String> deletePermission(@RequestParam("idSender") Integer idSender,@RequestParam("idReciver") Integer idReciver) {
		permissionQueue.removeIf(notifi -> (notifi.getIdsender().equals(idSender) && notifi.getIdreceiver().equals(idReciver)));
		return ResponseEntity.ok("Deleted");
	}
	
	@PostMapping("/StorePermissionResp")
	public ResponseEntity<String> storePermissionResp(@RequestParam("idSender") Integer idSender,@RequestParam("idReciver") Integer idReciver, @RequestParam("value") String value,@RequestParam("senderName") String senderName,@RequestParam("receiverName") String receiverName) {
	 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	 	Date date = new Date(); 
		permissionQueue.add(new AccNotification(null, idSender, idReciver, AccType.permissionResp, value,formatter.format(date),senderName,receiverName));
		return ResponseEntity.ok("Permission Seneded Successfully");

	}
	

	@GetMapping("/getPermissionResp")
	public ResponseEntity<String> getPermissionResp(@RequestParam("idSender") Integer idSender,@RequestParam("idReciver") Integer idReciver) {
		boolean check = permissionQueue.stream()
			    .filter(notify -> notify.getIdsender().equals(idSender) && notify.getIdreceiver().equals(idReciver) && notify.getNotificationType()==AccType.permissionResp)
			    .findFirst().isPresent();
		if(check) {
		AccNotification notif = permissionQueue.stream()
				    .filter(notify -> notify.getIdsender().equals(idSender) && notify.getIdreceiver().equals(idReciver) && notify.getNotificationType()==AccType.permissionResp)
				    .findFirst().get();
		accNotificationRepository.save(notif);
		permissionQueue.removeIf(notifi -> (notifi.getIdsender().equals(idSender) && notifi.getIdreceiver().equals(idReciver))&& notifi.getNotificationType()==AccType.permissionResp);
		permissionQueue.removeIf(notifi -> (notifi.getIdsender().equals(idSender) && notifi.getIdreceiver().equals(idReciver))&& notifi.getNotificationType()==AccType.permission);
		return ResponseEntity.ok(notif.getValue());
		}
		return ResponseEntity.ok("Waiting for Response");
	}
	@GetMapping("/getAllAccountNotification")
	private ResponseEntity<?> getAllAccountNotificationById(@RequestParam("id") Integer num){
		return ResponseEntity.ok(new Gson().toJson(accNotificationRepository.findByIdsenderOrIdreceiver(num,num)));
	}
	@GetMapping("/getAllBoardNotification")
	private ResponseEntity<?> getAllNotificationByIdBoard(@RequestParam("id") String num){
		return ResponseEntity.ok(new Gson().toJson(notificationRepository.findByIdBoard(num)));
	}
}
