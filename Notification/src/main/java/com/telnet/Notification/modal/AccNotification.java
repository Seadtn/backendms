package com.telnet.Notification.modal;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccNotification {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer idsender;
	private Integer idreceiver;
	@Enumerated(EnumType.STRING)
	private AccType notificationType; 
	@Column(columnDefinition = "LONGTEXT") 
	private String value;
	private String time;
	private String senderName;
	private String receiverName;
    public enum AccType {
    	permission,
    	permissionResp,
    	newDevice,
    	newDeviceResp,
    }
}
