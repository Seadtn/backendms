package com.telnet.Mqtt.modal;


import org.eclipse.paho.client.mqttv3.MqttClient;
import org.springframework.data.redis.core.RedisHash;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
@Data
@RedisHash("CLIENTV3")
@AllArgsConstructor
@ToString
public class ConnectedClientv3 {
	private transient  MqttClient mqttClient;
	private int number;
	private MqttAgent mqttAgent;
}
