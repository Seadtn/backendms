package com.telnet.Mqtt.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.protobuf.ByteString;

import com.telnet.Mqtt.modal.ConnectedClientv3;
import com.telnet.Mqtt.repository.MqttV3Repository;

import usp_record.UspRecord13.Record;
@Service
public class MqttV3MethodsImp implements MqttMethodesV3 {
	@Autowired
	MqttV3Repository mqttV3Repository;
	public static List<MqttClient> clientsv3= new ArrayList<MqttClient>();
	@Override
	public ConnectedClientv3 GetConnectedClient(String idClient) {
		MqttClient client=null;
		for (MqttClient mc : clientsv3) {
			if(mc.getClientId().equals(idClient)) {
				client=mc;
				break;
			}
		}
		ConnectedClientv3 clientv3= mqttV3Repository.findById(idClient);
		clientv3.setMqttClient(client);
		return  clientv3;
	}
	@Override
	public void publish(byte[] msg, String topic ,String replyTopic, boolean retain, MqttClient client, int qos)
			throws JsonProcessingException {
		try {

			MqttMessage message = new MqttMessage(msg);
			message.setQos(qos);
			message.setRetained(retain);
		    String PublishTopic = topic + "/reply-to=" + replyTopic;
			client.publish(PublishTopic, message);
			System.out.println("MQTTV3 : Published to " + topic);
		} catch (MqttException e) {

			e.printStackTrace();

		}

	}

	@Override

	public ByteString waitForResponse(String replyTopic, MqttClient mqttClient, int qos, byte[] tr369Message)
			throws MqttException, InterruptedException, JsonProcessingException {
		CountDownLatch responseReceivedLatch = new CountDownLatch(5);
		mqttClient.subscribe(replyTopic, qos);
		publish(tr369Message,"agent/" + mqttClient.getClientId(),replyTopic, true, mqttClient, 0);
		ByteString resp = addCallback(mqttClient, responseReceivedLatch);
		responseReceivedLatch.await(1, TimeUnit.SECONDS);
		return resp;
	}

	private ByteString addCallback(MqttClient mqttClient, CountDownLatch responseReceivedLatch) {
		CompletableFuture<ByteString> future = new CompletableFuture<>();
		mqttClient.setCallback(new MqttCallback() {
			@Override
			public void messageArrived(String topic, MqttMessage message) throws Exception {
				Record record = Record.parseFrom(message.getPayload());
				future.complete(record.getNoSessionContext().getPayload());
				responseReceivedLatch.countDown();
			}
			@Override
			public void connectionLost(Throwable cause) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken token) {
				// TODO Auto-generated method stub
				
			}
		});
		try {
			return future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;
		}

	}
	@Override
	public void RemoveClient(String idClient) throws MqttException {
		ConnectedClientv3 clientv3= GetConnectedClient(idClient);
		if(clientv3.getNumber()>1) {
			clientv3.setNumber(clientv3.getNumber()-1);
			mqttV3Repository.update(clientv3);
		}else {
			clientv3.getMqttClient().disconnect();
			clientsv3.removeIf(client-> client.getClientId().equals(idClient));
			mqttV3Repository.removeById(idClient);
		}
	}
}
