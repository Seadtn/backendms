package com.telnet.Notification.WsConfig.service;


import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.telnet.Notification.kafka.listener.KafkaListeners;
import com.telnet.Notification.modal.WebSession;

import lombok.AllArgsConstructor;
import lombok.Data;

@Component
@Data
@AllArgsConstructor
public class WebSocketListener {
		@EventListener
		public void HandleWebSocketDisconnectListener(SessionDisconnectEvent disconnectEvent) {
			 StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(disconnectEvent.getMessage());
			 String jsonStompHeader= new Gson().toJson(headerAccessor);
			 JsonObject jsonObject = new Gson().fromJson(jsonStompHeader, JsonObject.class);
			 String idsession = jsonObject
					    .getAsJsonObject("headers")
					    .getAsJsonObject("simpUser")
					    .getAsJsonPrimitive("username")
					    .getAsString();
			 
			 if(idsession!=null) {
				 KafkaListeners.webSessions.removeIf(session -> session.getSessionId().equals(idsession));
			 }
		}
		
		
		@EventListener
		public void HandleWebSocketConnectListener(SessionConnectedEvent connectedEvent) {
			 StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(connectedEvent.getMessage());
			 String jsonStompHeader= new Gson().toJson(headerAccessor);
			 JsonObject jsonObject = new Gson().fromJson(jsonStompHeader, JsonObject.class);
			 String idsession =jsonObject
					    .getAsJsonObject("headers")
					    .getAsJsonObject("simpUser")
					    .getAsJsonPrimitive("username")
					    .getAsString();	
			 String iduser = jsonObject
					    .getAsJsonObject("headers")
					    .getAsJsonObject("simpConnectMessage")
					    .getAsJsonObject("headers")
					    .getAsJsonObject("nativeHeaders")
					    .getAsJsonArray("userName")
					    .getAsString();
			 if(iduser!=null) {
				WebSession session = new WebSession(idsession,iduser);
				KafkaListeners.webSessions.add(session);
			 }
			 
		}

	
	
}
