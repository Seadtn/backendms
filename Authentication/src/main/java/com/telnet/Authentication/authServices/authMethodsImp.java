package com.telnet.Authentication.authServices;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.protobuf.ByteString;
import com.telnet.Authentication.clients.MqttAuthClient;
import com.telnet.Authentication.clients.PermissionClient;
import com.telnet.Authentication.modal.Account;
import com.telnet.Authentication.modal.Agent;
import com.telnet.Authentication.modal.MQTTVersion;
import com.telnet.Authentication.modal.MqttAgent;
import com.telnet.Authentication.modal.MqttConnectRequest;
import com.telnet.Authentication.modal.Status;
import com.telnet.Authentication.modal.UserRole;
import com.telnet.Authentication.repository.AccountRepository;
import com.telnet.Authentication.repository.AgentRepository;
import com.telnet.Authentication.repository.MqttRepository;
import lombok.RequiredArgsConstructor;
import usp.UspMsg13.Msg;
import usp.UspMsg13.Notify.OnBoardRequest;


@Service
@RequiredArgsConstructor
public class authMethodsImp implements authMethods {
	public static int idMsg = 1;
	@Value("${broker.mqtt.username}")
	private String brokerUserName;
	@Value("${broker.mqtt.password}")
	private String brokerPassword;
	@Value("${broker.mqtt.qos}")
	private int qos;
	@Value("${broker.mqtt.tls}")
	private boolean tls;
	@Value("${broker.mqtt.url}")
	private String mqttBrokerURL;

	private final AccountRepository accountRepository;
	private final  MqttAuthClient mqttAuthClient;
	private final  MqttRepository mqttRepository;
	private final  AgentRepository agentRepository;
	private final  PermissionClient prClient;
	@Autowired
    private KafkaTemplate<String, String> kafkaTemplate;


	
	@Override
	public String generateRandomPassword(int len) {
		final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		SecureRandom random = new SecureRandom();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {

			int randomIndex = random.nextInt(chars.length());

			sb.append(chars.charAt(randomIndex));

		}
		return sb.toString();
	}

	@Override
	public ByteString CheckMQTTRegister(String version, String id, String controllerId,Integer idUser) throws Exception {
		if (mqttRepository.findById(id).isPresent()) {

			return ByteString.copyFromUtf8("Board Exist ! \n");
		}
		ByteString responseData = null;
		
		if (version.equals("v3_1_1") || version.equals("v3") || version.equals("v5")) {
			responseData=ByteString.copyFrom(mqttAuthClient.CheckRegister(version, id, qos, mqttBrokerURL, brokerUserName, brokerPassword, controllerId,idUser).getBody());
				if (responseData == null || responseData.isEmpty()) {
					return ByteString.copyFromUtf8("Board not connected to the broker");
				}
		} else {
			return ByteString.copyFromUtf8("Version not supported! \n " + "supported versions are [ v3, v3_1_1 ,v5]\n");
		}
		return responseData;
	}

	@Override
	public ResponseEntity<String> CheckRegisterProtocol(String protocol, String version, String id,
			String controllerId, Account account) throws Exception {
		if (protocol.toLowerCase().equals("mqtt")) {
			ByteString result = CheckMQTTRegister(version, id, controllerId,account.getId());
			if (result.toStringUtf8().contains("Exist")) {
				return ResponseEntity.status(HttpStatus.FOUND).body(result.toStringUtf8());
			} else if (result.toStringUtf8().contains("Version not supported")) {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body(result.toStringUtf8());
			} else if (result.toStringUtf8().contains("Board not connected")){
				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(result.toStringUtf8());
			}else {
				Msg msg = Msg.parseFrom(result);
				OnBoardRequest resp = msg.getBody().getRequest().getNotify().getOnBoardReq();

				return SaveNewMqttAgent(id, resp.getAgentSupportedProtocolVersions(), "controller",
						generateRandomPassword(10), protocol, controllerId,
						MQTTVersion.valueOf(version),account);
			}
		} else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Protocol not supported \n "
					+ "Supported Protocols are [mqtt/MQTT , stomp/STOMP , uds/UDS , ws/WS]\n");
		}
	}

	@Override
	public ResponseEntity<String> SaveNewMqttAgent(String id, String version, String endpointID,
			String password,String protocol, String controllerId, MQTTVersion mqttversion,
			Account account)
			throws JsonProcessingException, MqttException, InterruptedException {
		 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		 	Date date = new Date();  
			Account acc=accountRepository.findById(account.getId()).get();
			List<Agent> lsagents=acc.getListAgents();
			MqttAgent agent = new MqttAgent(id, version, endpointID,protocol,mqttBrokerURL ,brokerUserName, brokerPassword, controllerId,
				mqttversion, qos, tls,formatter.format(date),"Device not updated yet",formatter.format(date),Status.Online,account,"No Client is Connected");
			lsagents.add(agent);
			acc.setListAgents(lsagents);
			mqttRepository.save(agent);
			accountRepository.save(acc);
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("username", agent.getAccount().getUsername());
			jsonObject.addProperty("IdSender", agent.getAccount().getId());
			jsonObject.addProperty("boardId", agent.getIdBoard());
			kafkaTemplate.send("new-device", new Gson().toJson(jsonObject));
			return ResponseEntity.status(HttpStatus.OK).body("Created !");
	}

	@Override
	public ResponseEntity<String> CheckMQTTConnection(MqttAgent mqttAgent, Account account)
			throws JsonProcessingException, MqttException, InterruptedException,
			org.eclipse.paho.client.mqttv3.MqttException {
		CheckMqttParameters(mqttAgent,mqttBrokerURL,brokerUserName, brokerPassword, tls, qos);
		MqttConnectRequest connectRequest = new MqttConnectRequest(
			    mqttAgent.getIdBoard(),
			    mqttAgent.getUspVersions(),
			    mqttAgent.getEndpointId(),
			    mqttAgent.getProtocol(),
			    mqttAgent.getBrokerUrl(),
			    mqttAgent.getBrokerUserName(),
			    mqttAgent.getBrokerPassword(),
			    mqttAgent.getControllerId(),
			    mqttAgent.getMqttVersion(),
			    qos,
			    tls
			);	
		if(account.getRole()!=UserRole.Admin) {
			String connectedClients=mqttAgent.getConnectedClients();
			if(connectedClients.equals("No Client is Connected")) {
				if(mqttAgent.getAccount().getUsername().equals(account.getUsername())) {
					connectedClients=account.getUsername()+"/Owner";
				}else {
					connectedClients=account.getUsername();
				}
			}else {
				if(mqttAgent.getAccount().getUsername().equals(account.getUsername())) {
					connectedClients=connectedClients.concat(","+account.getUsername()+"/Owner");
				}else {
					connectedClients=connectedClients.concat(","+account.getUsername());
				}
			}
			mqttAgent.setConnectedClients(connectedClients);
			agentRepository.save(mqttAgent);
		}
		String jsonObject = new Gson().toJson(connectRequest);
		mqttAuthClient.CheckConnection(jsonObject, mqttAgent.getMqttVersion().name(), qos);
		return ResponseEntity.status(HttpStatus.OK).body( "Connect to the board "+mqttAgent.getIdBoard());
	}

	@Override
	public ResponseEntity<String> CheckConnectionProtocol(String idBoard,Account account)
			throws JsonProcessingException, MqttException, InterruptedException,
			org.eclipse.paho.client.mqttv3.MqttException {
		Agent agent = agentRepository.findById(idBoard).get();
		if((agent.getAccount().getId()!=account.getId()) && account.getRole()!=UserRole.Admin && agent.getAccount().getStatus()==Status.Online) {
			String msg=prClient.sendPermission(account.getId(), agent.getAccount().getId(),account.getUsername(),agent.getIdBoard(),agent.getAccount().getUsername()).getBody();
			if(msg.startsWith("Sending....")) {
				String resp = "Waiting for Response";
				long t= System.currentTimeMillis();
				long end = t+30000;
				while(System.currentTimeMillis() < end && resp.equals("Waiting for Response")) {
				    resp = prClient.getPermissionResp(account.getId(), agent.getAccount().getId()).getBody();
				    Thread.sleep(1000);
				}

				if (resp.equals("Waiting for Response") || resp.equals("Denied") ) {
					prClient.deletePermission(account.getId(), agent.getAccount().getId());				    
					return ResponseEntity.status(HttpStatus.FORBIDDEN).body( agent.getAccount().getUsername() + " doesn't accept your request");
				}
	
			}else if (msg.contains("Denied: You are connecting") || msg.contains("The client you're attempting to connect")) {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body(msg);
			}
		}
		if (agent.getProtocol().toLowerCase().equals("mqtt")) {
			return CheckMQTTConnection((MqttAgent)agent,account);
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("ERROR !");
	}

	@Override
	public ResponseEntity<String> CheckDisconnectionProtocol(String protocol,String version ,String idclient,String username)
			throws JsonProcessingException, MqttException, InterruptedException,
			org.eclipse.paho.client.mqttv3.MqttException {
		if (protocol.toLowerCase().equals("mqtt")) {
			DisconnectMQTTAgent(idclient, version,username);
			return ResponseEntity.ok("Successfully disconnected from MQTT broker.");
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("ERROR !");
	}

	@Override
	public void DisconnectMQTTAgent(String idclient,String version,String username) throws JsonProcessingException, MqttException,
			InterruptedException, org.eclipse.paho.client.mqttv3.MqttException {
		if(!username.contains("Admin")) {
		Agent agent = agentRepository.findById(idclient).get();
		String connectedClients = agent.getConnectedClients();
		if(connectedClients.contains(",")) {
			List<String> usernames =  new ArrayList<String>(Arrays.asList(connectedClients.split(",")));
			System.out.println(usernames.toString());
			for(String name : usernames) {
				if(name.contains("/Owner")) {
					String ownername= name.split("/")[0];
					if(ownername.equals(username)) {
						usernames.remove(name);
						break;
					}
				}else {
					if(name.equals(username)) {
						usernames.remove(name);
						break;
					}
				}
			}
			if(usernames.isEmpty()) {
				connectedClients ="No Client is Connected";
			}else {
				connectedClients = String.join(",", usernames);
	
			}
		}else {
			connectedClients ="No Client is Connected";
		}
		agent.setConnectedClients(connectedClients);
		agentRepository.save(agent);
		}
		mqttAuthClient.Disconnect(idclient, version);
	}

	@Override
	public void CheckMqttParameters(MqttAgent mqttAgent,String brokerurl, String username, String password,
			boolean tls, int qos) {
		if (!mqttAgent.getBrokerUrl().equals(brokerurl)) {
			mqttAgent.setBrokerUrl(brokerurl);
		}
		if (!mqttAgent.getBrokerUserName().equals(username) || !mqttAgent.getBrokerPassword().equals(password)
			|| mqttAgent.isTls() != tls || mqttAgent.getQos() != qos) {
			if (mqttAgent.isTls() != tls) {
				mqttAgent.setTls(tls);
			}
			if (mqttAgent.getQos() != qos) {
				mqttAgent.setQos(qos);
			}
			mqttRepository.save(mqttAgent);
		}

	}

}
