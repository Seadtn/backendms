package com.telnet.Mqtt.modal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class History {
	private Integer id;
	private	String request;
	private String response;
	private String status;
	private String type;
	private String time;
	private String idBoard;
	private Integer idClient;
	
}
