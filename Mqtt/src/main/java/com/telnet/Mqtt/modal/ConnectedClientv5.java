package com.telnet.Mqtt.modal;


import org.eclipse.paho.mqttv5.client.MqttClient;
import org.springframework.data.redis.core.RedisHash;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@RedisHash("CLIENTV5")
@AllArgsConstructor
@ToString
public class ConnectedClientv5{
	
	private transient  MqttClient mqttClient;
	
	private int number;
	
	private MqttAgent mqttAgent;
}
