package com.telnet.Notification.kafka.listener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.telnet.Notification.WsConfig.service.WebSocketService;
import com.telnet.Notification.modal.AccNotification;
import com.telnet.Notification.modal.AccNotification.AccType;
import com.telnet.Notification.modal.Notification;
import com.telnet.Notification.modal.WebSession;
import com.telnet.Notification.repository.AccNotificationRepository;
import com.telnet.Notification.repository.NotificationRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class KafkaListeners {
	private final AccNotificationRepository accNotificationRepository;
	private final NotificationRepository notificationRepository;
	public static List<WebSession> webSessions = new ArrayList<>();
	
	private final WebSocketService service;
	private final ObjectMapper objectMapper = new ObjectMapper();
	@KafkaListener(topics = {"mqtt-notification","stomp-notification","ws-notification","uds-notification"}, groupId = "notify")
	void listener(Message<String> message) {
		try {
		String notificaitonJson = message.getPayload();
        Notification notification = objectMapper.readValue(notificaitonJson, new TypeReference<Notification>() {});
        //TODO save notification
        notificationRepository.save(notification);
        List<WebSession>  sessions=getSessionId(notification.getIdBoard(),false);
		if(sessions!=null && !sessions.isEmpty()) {
			sessions.forEach(s->{
				service.notifyUser(s.getSessionId(),"/topic/notification",ResponseEntity.ok(notification));
			});
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	public List<WebSession> getSessionId(String idBoard , boolean getJustAdmin) {
		List<WebSession> lws= new ArrayList<>();
			if(getJustAdmin) {
				webSessions.forEach(session-> {
					if(session.getUserId().contains("admin")) {
						lws.add(session);
					}
				});
			}else {
				webSessions.forEach(session-> {
					String id = session.getUserId();
					if(id.contains(",")) {
						String idb= id.split(",")[1];
						if(idb.equals(idBoard)) {
							lws.add(session);
						}
					}
				});
			}
			return lws;
	}
	@KafkaListener(topics = {"new-device"}, groupId = "newDevice")
	void newDevicelistener(Message<String> message) {
		try {
		String notificaitonJson = message.getPayload();
		//TODO save notification
		JsonObject jsonObject = new Gson().fromJson(notificaitonJson, JsonObject.class);
	 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	 	Date date = new Date();  
		AccNotification notif = new AccNotification(null, jsonObject.get("IdSender").getAsInt(), 1, AccType.newDevice,"user " + jsonObject.get("username").getAsString() + " added a new device with id " + jsonObject.get("boardId").getAsString(),formatter.format(date), jsonObject.get("username").getAsString(),null);
		accNotificationRepository.save(notif);
        List<WebSession>  sessions=getSessionId(null,true);
		if(sessions!=null && !sessions.isEmpty()) {
			sessions.forEach(s->{
				service.notifyUser(s.getSessionId(),"/topic/notification",ResponseEntity.ok(notif));
			});
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@KafkaListener(topics = {"account-lock"}, groupId = "AccountLocked")
	void accountLocklistener(Message<String> message) {
		try {
		String id = message.getPayload();
		List<WebSession>  sessions= new ArrayList<>();
		webSessions.forEach(session-> {
				if(session.getUserId().startsWith(id)) {
					sessions.add(session);
				}
		});
		if(sessions!=null && !sessions.isEmpty()) {
			sessions.forEach(s->{
				service.notifyUser(s.getSessionId(),"/topic/notification",ResponseEntity.ok("The admin has locked this account. Please contact them."));
			});
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@KafkaListener(topics = {"force-disconnect"}, groupId = "forceDisconnect")
	void forceDisconnectlistener(Message<String> message) {
		String parametersJson = message.getPayload();
		JsonObject parameters = new Gson().fromJson(parametersJson, JsonObject.class);
		String idUser = parameters.get("idUser").getAsString();
		String idBoard = parameters.get("idBoard").getAsString();
		boolean owner =parameters.get("owner").getAsBoolean();
		List<WebSession>  sessions= new ArrayList<>();
		if(owner) {
			webSessions.forEach(session-> {
				if(session.getUserId().contains(",")) {
					if(session.getUserId().split(",")[1].equals(idBoard)) {
						sessions.add(session);
					}
				}
		});
		}else {
			webSessions.forEach(session-> {
				if(session.getUserId().contains(",")) {
					if(session.getUserId().split(",")[0].equals(idUser) && session.getUserId().split(",")[1].equals(idBoard)) {
						sessions.add(session);
					}
				}
		});
		}
		if(sessions!=null && !sessions.isEmpty()) {
			sessions.forEach(s->{
				service.notifyUser(s.getSessionId(),"/topic/notification",ResponseEntity.ok("The admin has blocked the acess to this board "+idBoard));
			});
		}
	
	}
}
