package com.telnet.Mqtt.repository;

import com.google.gson.Gson;
import com.telnet.Mqtt.modal.ConnectedClientv5;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MqttV5Repository {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private static final String KEY = "CLIENTV5";
    Gson gson = new Gson();
    public void save(ConnectedClientv5 clientv5) {
        try {
            String clientv5Json = gson.toJson(clientv5);
            System.out.println(clientv5Json);
            redisTemplate.opsForHash().put(KEY, clientv5.getMqttAgent().getIdBoard(), clientv5Json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ConnectedClientv5 findById(String id) {
        try {
            String clientv5Json = (String) redisTemplate.opsForHash().get(KEY, id);
            return gson.fromJson(clientv5Json, ConnectedClientv5.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(ConnectedClientv5 clientv5) {
        save(clientv5); 
    }

    public void removeById(String id) {
        redisTemplate.opsForHash().delete(KEY, id);
    }
}