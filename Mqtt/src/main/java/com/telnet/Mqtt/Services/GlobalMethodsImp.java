package com.telnet.Mqtt.Services;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.paho.mqttv5.client.MqttClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.protobuf.ByteString;
import com.telnet.Mqtt.modal.ConnectedClientv3;
import com.telnet.Mqtt.modal.ConnectedClientv5;
import com.telnet.Mqtt.modal.Error;
import com.telnet.Mqtt.modal.MqttAgent;
import com.telnet.Mqtt.modal.Notification;
import com.telnet.Mqtt.modal.Notification.NotificationType;
import com.telnet.Mqtt.modal.ResultResponse;
import com.telnet.Mqtt.modal.USPRequest;
import com.telnet.Mqtt.repository.ErrorRepository;
import com.telnet.Mqtt.repository.MqttV3Repository;
import com.telnet.Mqtt.repository.MqttV5Repository;

import lombok.RequiredArgsConstructor;
import usp.UspMsg13.Add;
import usp.UspMsg13.Body;
import usp.UspMsg13.Delete;
import usp.UspMsg13.Get;
import usp.UspMsg13.GetInstances;
import usp.UspMsg13.GetSupportedDM;
import usp.UspMsg13.GetSupportedProtocol;
import usp.UspMsg13.Header;
import usp.UspMsg13.Msg;
import usp.UspMsg13.Notify;

import usp.UspMsg13.Operate;
import usp.UspMsg13.Request;
import usp.UspMsg13.Set;
import usp.UspMsg13.Add.CreateObject;
import usp.UspMsg13.Header.MsgType;
import usp.UspMsg13.Set.UpdateObject;
import usp_record.UspRecord13.NoSessionContextRecord;
import usp_record.UspRecord13.Record;
import usp_record.UspRecord13.Record.PayloadSecurity;

@Service
@RequiredArgsConstructor
public class GlobalMethodsImp implements GlobalMethods {
	public static int idMsg = 1;
	
	private final MqttMethodesV5 mqttMethodesV5;
	private final MqttMethodesV3 mqttMethodesV3;
	private final  MqttV5Repository mqttV5Repository;
	private final MqttV3Repository mqttV3Repository;
	private final ErrorRepository errorRepository;
	@Override
	public USPRequest CreateUSPRecord(MsgType msgType, String idClient, HashMap<String, String> Parameters, boolean json)
			throws Exception {
		Request request = CreateRequest(msgType, Parameters, json);
		Header header = Header.newBuilder().setMsgId("" + idMsg).setMsgType(msgType).build();
		idMsg++;
		Body body = Body.newBuilder().setRequest(request).build();
		Msg msg = Msg.newBuilder().setBody(body).setHeader(header).build();
		byte[] msgByte = msg.toByteArray();
		ByteString byteString = ByteString.copyFrom(msgByte);
		NoSessionContextRecord noSessionContextRecord = NoSessionContextRecord.newBuilder().setPayload(byteString)
				.build();
		Record record = Record.newBuilder().setToId(idClient).setFromId("controller")
				.setPayloadSecurity(PayloadSecurity.PLAINTEXT).setNoSessionContext(noSessionContextRecord).build();
		byte[] tr369Message = record.toByteArray();
		return new USPRequest(tr369Message,new Gson().toJson(request));

	}

	@Override
	public Request CreateRequest(MsgType msgType, HashMap<String, String> Parameters, boolean json) throws Exception {
		Request request = null;
		switch (msgType) {
		case GET:
			Get get=null;
			if(json) {
		        JsonObject paramPathsObject = new Gson().fromJson(Parameters.get("json"), JsonObject.class);
		        JsonObject getObject = paramPathsObject.getAsJsonObject("get");
				get =  new Gson().fromJson(getObject, Get.class);
			}else {
			JsonArray jsonArrayGet = new Gson().fromJson(Parameters.get("paramPaths"), JsonArray.class);
			List<String> list= new ArrayList<>();
			for(JsonElement jsonElement : jsonArrayGet) {
				list.add(jsonElement.getAsString());
			}
			get=Get.newBuilder().addAllParamPaths(list)
			.setMaxDepth(Integer.parseInt(Parameters.get("maxDepth"))).build();
			}
			request = Request.newBuilder().setGet(get).build();
			break;
		case GET_INSTANCES:
			GetInstances getInstances=null;
			if(json) {
		        JsonObject paramPathsObject = new Gson().fromJson(Parameters.get("json"), JsonObject.class);
		        JsonObject getInsObject = paramPathsObject.getAsJsonObject("get_instances");
				getInstances =  new Gson().fromJson(getInsObject, GetInstances.class);
			}else {
			 getInstances = GetInstances.newBuilder().addAllObjPaths(Arrays.asList(Parameters.get("objPaths").split(",")))
							.setFirstLevelOnly(Boolean.parseBoolean(Parameters.get("firstLevelOnly"))).build();
			 }
			request = Request.newBuilder().setGetInstances(getInstances).build();
			break;
		case GET_SUPPORTED_DM:
			GetSupportedDM getSupportedDM = null;
			if(json) {
		        JsonObject paramPathsObject = new Gson().fromJson(Parameters.get("json"), JsonObject.class);
		        JsonObject getsuppdmObject = paramPathsObject.getAsJsonObject("get_supported_dm");
		        getSupportedDM =  new Gson().fromJson(getsuppdmObject, GetSupportedDM.class);
			}else {
			getSupportedDM = GetSupportedDM.newBuilder()
					.addAllObjPaths(Arrays.asList(Parameters.get("objPaths").split(",")))
					.setFirstLevelOnly(Boolean.parseBoolean(Parameters.get("firstLevelOnly")))
					.setReturnCommands(Boolean.parseBoolean(Parameters.get("returnCommands")))
					.setReturnEvents(Boolean.parseBoolean(Parameters.get("returnEvents")))
					.setReturnParams(Boolean.parseBoolean(Parameters.get("returnParams"))).build();
			}
			request = Request.newBuilder().setGetSupportedDm(getSupportedDM).build();
			break;
		case GET_SUPPORTED_PROTO:
			GetSupportedProtocol getSupportedProtocol = GetSupportedProtocol.newBuilder().build();
			request = Request.newBuilder().setGetSupportedProtocol(getSupportedProtocol).build();
			break;

		case SET:
			Set set= null;
			if(json) {
				JsonObject jsonObject = new Gson().fromJson(Parameters.get("json"), JsonObject.class);
				JsonObject setObject = jsonObject.getAsJsonObject("set");		        
				set =  new Gson().fromJson(setObject, Set.class);
			}else {
				JsonObject jsonObjectSet = new Gson().fromJson(Parameters.get("paramSettings"), JsonObject.class);
				JsonArray objectSetArray = jsonObjectSet.getAsJsonArray("paramSettings");
				List<UpdateObject> UpdateObjectSet = new ArrayList<UpdateObject>();
				for (JsonElement jsonElem : objectSetArray) {
					UpdateObject object = new Gson().fromJson(jsonElem, UpdateObject.class);
					UpdateObjectSet.add(object);
				}
				 set = Set.newBuilder().setAllowPartial(Boolean.parseBoolean(Parameters.get("allowPartial")))
						.addAllUpdateObjs(UpdateObjectSet).build();
			}

			request = Request.newBuilder().setSet(set).build();
			break;
		case ADD:
			Add add=null;
			if(json) {
				JsonObject jsonObject = new Gson().fromJson(Parameters.get("json"), JsonObject.class);
				JsonObject addObject = jsonObject.getAsJsonObject("add");		        
				add =  new Gson().fromJson(addObject, Add.class);
			}else {
			JsonObject jsonObjectAdd = new Gson().fromJson(Parameters.get("paramSettings"), JsonObject.class);
			JsonArray objectAddArray = jsonObjectAdd.getAsJsonArray("paramSettings");
			List<CreateObject> UpdateObjectAdd = new ArrayList<CreateObject>();
			for (JsonElement jsonElem : objectAddArray) {
				CreateObject paramSetting = new Gson().fromJson(jsonElem, CreateObject.class);
				UpdateObjectAdd.add(paramSetting);
			}
			 add = Add.newBuilder().setAllowPartial(Boolean.parseBoolean(Parameters.get("allowPartial")))
					.addAllCreateObjs(UpdateObjectAdd).build();
			}
			request = Request.newBuilder().setAdd(add).build();
			break;
		case DELETE:
			Delete delete=null;
			if(json) {
		        JsonObject objPathsObject = new Gson().fromJson(Parameters.get("json"), JsonObject.class);
				JsonObject deleteObject = objPathsObject.getAsJsonObject("delete");		        
		        delete =  new Gson().fromJson(deleteObject, Delete.class);
			}else {
			 delete = Delete.newBuilder().setAllowPartial(Boolean.parseBoolean(Parameters.get("allowPartial")))
					.addAllObjPaths(Arrays.asList(Parameters.get("objPaths").split(","))).build();
			}
			request = Request.newBuilder().setDelete(delete).build();
			break;
		case ERROR:
			break;
		case DEREGISTER:
			break;
		case OPERATE:
			Operate operate=null;
			if(json) {
		        JsonObject paramPathsObject = new Gson().fromJson(Parameters.get("json"), JsonObject.class);
				JsonObject operateObject = paramPathsObject.getAsJsonObject("operate");	
		        operate =  new Gson().fromJson(operateObject, Operate.class);
			}else {
			    Map<String, String> inputArgs = new HashMap<String, String>();
				if(!Parameters.get("inputArgs").trim().isEmpty()) {
					JsonObject inputs = new Gson().fromJson(Parameters.get("inputArgs"),JsonObject.class);
					if(inputs.size()>=0) {
					    java.util.Set<String> keys = inputs.keySet();
				        for (String key : keys) {
				            JsonPrimitive valuePrimitive = inputs.getAsJsonPrimitive(key);
				            inputArgs.put(key, valuePrimitive.getAsString());
				        }
			        }
		        }
				operate = Operate.newBuilder().setCommand(Parameters.get("command"))
					.setCommandKey(Parameters.get("commandKey"))
					.putAllInputArgs(inputArgs)
					.setSendResp(Boolean.parseBoolean(Parameters.get("sendResp"))).build();
			}
			request = Request.newBuilder().setOperate(operate).build();
			break;
		case REGISTER:
			break;
		case NOTIFY:
			break;
		default:
			break;
		}
		return request;
	}


	@Override
	public Notification checkNotificationType(Notify notify, String idBoard) {
	    NotificationType type = null;
	    String eventDescription = null;
	    if (!notify.getEvent().toString().isEmpty()) {
	        type = NotificationType.Event;
	        eventDescription =  new Gson().toJson(notify.getEvent());
	    } else if (!notify.getValueChange().toString().isEmpty()) {
	        type = NotificationType.ValueChange;
	        eventDescription = new Gson().toJson(notify.getValueChange());
	    } else if (!notify.getObjCreation().toString().isEmpty()) {
	        type = NotificationType.ObjectCreation;
	        eventDescription =  new Gson().toJson(notify.getObjCreation());
	    } else if (!notify.getObjDeletion().toString().isEmpty()) {
	        type = NotificationType.ObjectDeletion;
	        eventDescription =  new Gson().toJson(notify.getObjDeletion());
	    } else if (!notify.getOperComplete().toString().isEmpty()) {
	        type = NotificationType.OperationComplete;
	        eventDescription = new Gson().toJson(notify.getOperComplete());
	    } else if (!notify.getOnBoardReq().toString().isEmpty()) {
	        type = NotificationType.OnBoardRequest;
	        eventDescription = new Gson().toJson(notify.getOnBoardReq());
	    }
	 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	 	Date date = new Date(); 
	    return new Notification(null, idBoard, type, eventDescription,formatter.format(date));
	}

	@Override
	public void CheckConnectedAgent(MqttAgent mqttmodal, String version ,int qos) throws MqttException, org.eclipse.paho.client.mqttv3.MqttException {
		if (version.equals("v5")) {
			MqttClient mqttClient=null;
			ConnectedClientv5 mqttv5 = mqttV5Repository.findById(mqttmodal.getIdBoard());
			if(mqttv5!=null) {
				mqttv5.setNumber(mqttv5.getNumber()+1);
				mqttV5Repository.update(mqttv5);
			}else{
				mqttClient= new MqttClient(mqttmodal.getBrokerUrl(), mqttmodal.getIdBoard());
				ConnectedClientv5 clientv5= new ConnectedClientv5(mqttClient,1,mqttmodal);
				MqttV5MethodsImp.clientsv5.add(mqttClient);
				mqttV5Repository.save(clientv5);
				MqttConnectionOptions connOpts = new MqttConnectionOptions();
				connOpts.setCleanStart(true);
				connOpts.setUserName(mqttmodal.getBrokerUserName());
				connOpts.setPassword(mqttmodal.getBrokerPassword().getBytes());
				mqttClient.connect(connOpts);			}
			
		} else {
			org.eclipse.paho.client.mqttv3.MqttClient mqttClient=null;
			ConnectedClientv3 mqttv3 = mqttV3Repository.findById(mqttmodal.getIdBoard());
			if(mqttv3!=null) {
				mqttv3.setNumber(mqttv3.getNumber()+1);
				mqttV3Repository.update(mqttv3);
			}else{
				mqttClient= new org.eclipse.paho.client.mqttv3.MqttClient(mqttmodal.getBrokerUrl(), mqttmodal.getIdBoard());
				ConnectedClientv3 clientv3= new ConnectedClientv3(mqttClient,1,mqttmodal);
				MqttV3MethodsImp.clientsv3.add(mqttClient);
				mqttV3Repository.save(clientv3);
				org.eclipse.paho.client.mqttv3.MqttConnectOptions options = new org.eclipse.paho.client.mqttv3.MqttConnectOptions();
				options.setUserName(mqttmodal.getBrokerUserName());
				options.setPassword(mqttmodal.getBrokerPassword().toCharArray());
				options.setCleanSession(true);
				mqttClient.connect(options);
			}
		}
		
	}

	@Override
	public void DisconnectAgent(String idBoard, String version) throws MqttException, org.eclipse.paho.client.mqttv3.MqttException {
		if (version.equals("v5")) {
			mqttMethodesV5.RemoveClient(idBoard);
		} else {
			mqttMethodesV3.RemoveClient(idBoard);
		}
		
	}

	@Override
	public ByteString Register(String controllerId,String version,String mqttBrokerURL,String id,String brokerUserName,String brokerPassword,int qos,Integer idUser) throws JsonProcessingException, InterruptedException, Exception {
		String command = "Device.LocalAgent.Controller." + controllerId + ".SendOnBoardRequest()";
		HashMap<String, String> Parameters = new HashMap<String, String>();
		Parameters.put("command", command);
		Parameters.put("commandKey", "onboard_command_key");
		Parameters.put("inputArgs", "");
		Parameters.put("sendResp", "false");
		ByteString responseData = null;
		if (version.equals("v5")) {
			MqttClient mqttClient = new MqttClient(mqttBrokerURL, id);
			MqttConnectionOptions options = new MqttConnectionOptions();
			options.setUserName(brokerUserName);
			options.setPassword(brokerPassword.getBytes());
			options.setCleanStart(true);
			mqttClient.connect(options);
			USPRequest request= CreateUSPRecord(MsgType.OPERATE, id, Parameters, false);
			responseData = mqttMethodesV5.waitForResponse("notify/"+id, mqttClient, qos,
					request.getRequestByte(),idUser,MsgType.OPERATE,request.getRequestString());
			mqttClient.unsubscribe("notify/"+id);
			mqttClient.disconnect();
		} else {
			org.eclipse.paho.client.mqttv3.MqttClient mqttClient = new org.eclipse.paho.client.mqttv3.MqttClient(
					mqttBrokerURL, id);
			org.eclipse.paho.client.mqttv3.MqttConnectOptions options = new org.eclipse.paho.client.mqttv3.MqttConnectOptions();
			options.setUserName(brokerUserName);
			options.setPassword(brokerPassword.toCharArray());
			options.setCleanSession(true);
			mqttClient.connect(options);
			USPRequest request= CreateUSPRecord(MsgType.OPERATE, id, Parameters, false);

			responseData = mqttMethodesV3.waitForResponse("notify/"+id, mqttClient, qos,
					request.getRequestByte());
			mqttClient.unsubscribe("notify/"+id);
			mqttClient.disconnect();
		}
		return responseData;
	}
	 private  JsonNode getErrorFields(int type, String data) {
		 JsonNode  errorFields = null;
	        ObjectMapper mapper = new ObjectMapper();
	        System.out.println(data);
	        try {
	        	JsonNode jsonData = mapper.readTree(data);
	            switch (type) {
	                case 2:
	                	//GET
	                	errorFields = jsonData.get("body_").get("msgBody_").get("respType_").get("reqPathResults_");
	                	break;
	                case 5:
	                	//SET
	                	errorFields = jsonData.get("body_").get("msgBody_").get("respType_").get("updatedObjResults_");
	                	break;
	                case 7:
	                    //Operate
	                	errorFields = jsonData.get("body_").get("msgBody_").get("respType_").get("operationResults_");
	                	break;
	                case 9:
	                    //ADD
	                	errorFields = jsonData.get("body_").get("msgBody_").get("respType_").get("createdObjResults_");
	                	break;
	                case 11:
	                    //Delete
	                	errorFields = jsonData.get("body_").get("msgBody_");
	                	break;
	                case 13:
	                    //GET DM
	                	errorFields = jsonData.get("body_").get("msgBody_").get("respType_").get("reqPathResults_");
	                	break; 
	                case 15:
	                    //GET INST
	                	errorFields = jsonData.get("body_").get("msgBody_").get("respType_").get("reqPathResults_");
	                	break;
	                case 18:
	                    //GET PROTO
	                	errorFields = jsonData.get("body_").get("msgBody_").get("respType_").get("reqPathResults_");
	                    break;
	                default:
	                	errorFields = jsonData.get("body_").get("msgBody_");

	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return errorFields;
	    }


		private String getError(JsonNode error , int type) {
			String errors =null;
            switch (type) {
            case 2:
            	//GET Done
            	errors = error.get("errCode_").asText() + "/" +  error.get("errMsg_").asText();
            	break;
            case 5:
            	//SET
            	JsonNode operStatus = error.get("operStatus_").get("operStatus_");    
            	if(operStatus.has("errCode_") && operStatus.has("errMsg_")) {
            		errors = operStatus.get("errCode_").asText() + "/" +  operStatus.get("errMsg_").asText();
            	}
            	break;
            case 7:
                //Operate
            	JsonNode opers = error.get("operationResp_");    
            	if(opers.has("errCode_") && opers.has("errMsg_")) {
            		errors = opers.get("errCode_").asText() + "/" +  opers.get("errMsg_").asText();
            	}
            	break;
            case 9:
                //ADD Done
            	JsonNode operStat =error.get("operStatus_").get("operStatus_");
            	if(operStat.has("errCode_") && operStat.has("errMsg_")) {
            		errors = operStat.get("errCode_").asText() + "/" +  operStat.get("errMsg_").asText();
            	}
            	break;
            case 11:
                //Delete
            	errors = error.get("errCode_").asText() + "/" +  error.get("errMsg_").asText();
            	break;
            case 13:
                //GET DM
            	errors = error.get("errCode_").asText() + "/" +  error.get("errMsg_").asText();
            	break; 
            case 15:
                //GET INST
            	errors = error.get("errCode_").asText() + "/" +  error.get("errMsg_").asText();
            	break;
            case 18:
                //GET PROTO
            	errors = error.get("errCode_").asText() + "/" +  error.get("errMsg_").asText();
                break;
            default:
            	errors = error.get("errCode_").asText() + "/" +  error.get("errMsg_").asText();

        }
			return errors;
		}
	@Override
	public ResultResponse checkIfHasError(Msg msg) {
	     JsonNode errorsFields = getErrorFields(msg.getHeader().getMsgTypeValue(),new Gson().toJson(msg));
	     List<Error> errors = new ArrayList<>();
	     List<Error> expectedErrors = new ArrayList<>();
	     List<Error> unknownErrors = new ArrayList<>();
        if (errorsFields == null) {
            return new ResultResponse(msg.toString(), errors, expectedErrors, unknownErrors);
        }
        try {
        	if(msg.getHeader().getMsgType()!=MsgType.ERROR) {
            for (JsonNode error : errorsFields) {
            	String requestedPath="";
            	if(msg.getHeader().getMsgTypeValue()!=7) {
            		 requestedPath = error.get("requestedPath_").asText();
                }else{
                	 requestedPath=error.get("executedCommand_").asText();
                }
                String es= getError(error, msg.getHeader().getMsgTypeValue());
                if(es!=null) {
                String errorCode = es.split("/")[0];
                if(!errorCode.equals("0")) {
                   String errorMessage = es.split("/")[1];
                   Error e =errorRepository.findById(errorCode).get();
	               if(e!=null) {
	            	   errors.add(new Error(errorCode, errorMessage, requestedPath, null));
	               }else {
	            	   Error errExp =errorRepository.findByErrorMsg(errorMessage);
	            	   if(errExp!=null) {
	            		   expectedErrors.add(new Error(errorCode, errorMessage, requestedPath, errExp.getErrorCode()));
	            	   }else if(errExp==null && !errorMessage.isEmpty() && !errorCode.isEmpty() ){
	            		   unknownErrors.add(new Error(errorCode,errorMessage,requestedPath,null));
	            	   }
	               }
               }
                }
            }}else {
                String es= getError(errorsFields, msg.getHeader().getMsgTypeValue());
                String errorCode = es.split("/")[0];
                if(!errorCode.equals("0")) {
                    String errorMessage = es.split("/")[1];
                    Error e =errorRepository.findById(errorCode).get();
 	               if(e!=null) {
 	            	   errors.add(new Error(errorCode, errorMessage, "", null));
 	               }else {
 	            	   Error errExp =errorRepository.findByErrorMsg(errorMessage);
 	            	   if(errExp!=null) {
 	            		   expectedErrors.add(new Error(errorCode, errorMessage, "", errExp.getErrorCode()));
 	            	   }else if(errExp==null && !errorMessage.isEmpty() && !errorCode.isEmpty() ){
 	            		   unknownErrors.add(new Error(errorCode,errorMessage,"",null));
 	            	   }
 	               }            
 	               }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new  ResultResponse(msg.toString(), errors, expectedErrors, unknownErrors);
	}
	


}
