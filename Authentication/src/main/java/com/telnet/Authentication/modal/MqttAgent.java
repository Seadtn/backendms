package com.telnet.Authentication.modal;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;


@Entity
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class MqttAgent extends Agent {
    private String brokerUrl;
    private String brokerUserName;
    private String brokerPassword;
    private String controllerId;
    @Enumerated(EnumType.STRING)
    private MQTTVersion mqttVersion;
    private int qos;
    private boolean tls;

 

    @Builder(builderMethodName = "agentBuilder")
    public MqttAgent(String idBoard, String uspVersions, String endpointId ,String protocol,
                     String brokerUrl, String brokerUserName, String brokerPassword, String controllerId, MQTTVersion mqttVersion,
                     int qos ,boolean tls,String createdTime,String lastUpdateTime,String lastContactTime,Status status,Account account,String connectedClients) {
        super(idBoard, uspVersions, endpointId,protocol,createdTime,lastUpdateTime,lastContactTime,connectedClients, account,status);
        this.brokerUrl = brokerUrl;
        this.brokerUserName = brokerUserName;
        this.brokerPassword = brokerPassword;
        this.controllerId = controllerId;
        this.mqttVersion = mqttVersion;
        this.qos = qos;
        this.tls = tls;
}
}
