package com.telnet.Mqtt.Services;

import java.util.HashMap;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.protobuf.ByteString;
import com.telnet.Mqtt.modal.MqttAgent;
import com.telnet.Mqtt.modal.Notification;
import com.telnet.Mqtt.modal.ResultResponse;
import com.telnet.Mqtt.modal.USPRequest;

import usp.UspMsg13.Header;
import usp.UspMsg13.Request;
import usp.UspMsg13.Header.MsgType;
import usp.UspMsg13.Msg;
import usp.UspMsg13.Notify;

@Service
public interface GlobalMethods {
	public USPRequest CreateUSPRecord(Header.MsgType msgType, String idClient, HashMap<String, String> Parameters,
			boolean json) throws Exception;

	public Request CreateRequest(MsgType msgType, HashMap<String, String> Parameters, boolean json) throws Exception;

	public Notification checkNotificationType(Notify notify, String idBoard);

	public void CheckConnectedAgent(MqttAgent mqttAgent, String version, int qos)
			throws MqttException, org.eclipse.paho.client.mqttv3.MqttException;

	public void DisconnectAgent(String idBoard, String version)
			throws MqttException, org.eclipse.paho.client.mqttv3.MqttException;
	public ResultResponse checkIfHasError(Msg msg) ;
	public ByteString Register(String controllerId,String version,String mqttBrokerURL,String id,String brokerUserName,String brokerPassword,int qos,Integer idUser) throws MqttException, JsonProcessingException, InterruptedException, Exception;
}
