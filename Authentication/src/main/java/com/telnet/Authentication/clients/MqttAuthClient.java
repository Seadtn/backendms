package com.telnet.Authentication.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
@Service
@FeignClient(name = "mqtt-service", url = "${application.config.mqtt.url}")
public interface MqttAuthClient {

	@PostMapping("/CheckConnection/{version}")
	void CheckConnection(@RequestBody String mqttAgent, @PathVariable("version") String version,
			@RequestParam("qos") int qos);

	@PostMapping("/CheckRegister/{version}")
	ResponseEntity<byte[]> CheckRegister(@PathVariable("version") String version, @RequestParam("idClient") String id,
			@RequestParam("qos") int qos, @RequestParam("mqttBrokerURL") String mqttBrokerURL,
			@RequestParam("brokerUserName") String brokerUserName,
			@RequestParam("brokerPassword") String brokerPassword, @RequestParam("controllerId") String controllerId,
			@RequestParam("idUser") Integer idUser);

	@PostMapping("/{version}/Disconnect/{idClient}")
	void Disconnect(@PathVariable("idClient") String idClient, @PathVariable("version") String version);
}
