package tn.telnet.History.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.telnet.History.modal.History;

public interface HistoryRepository extends JpaRepository<History, Integer> {
	List<History> findByIdClient(Integer id);
	List<History> findByIdBoard(String id);
	void deleteByIdBoard(String id);
}

