package com.telnet.Mqtt.Controller;


import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttSecurityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.telnet.Mqtt.Services.GlobalMethods;

import com.telnet.Mqtt.modal.MqttAgent;



@RestController
@RequestMapping("/mqtt/auth")
public class MqttAuthController {

	@Autowired
	private GlobalMethods globalMethods;
	
	@PostMapping("/CheckConnection/{version}")
	public void checkConnection(@RequestBody String object, @PathVariable("version") String version,
			@RequestParam("qos") int qos)
			throws MqttSecurityException, MqttException, org.eclipse.paho.client.mqttv3.MqttException {
	
		MqttAgent mqttAgent = new Gson().fromJson(object, MqttAgent.class);
		globalMethods.CheckConnectedAgent(mqttAgent, version, qos);
	}

	@PostMapping("/CheckRegister/{version}")
	public ResponseEntity<byte[]> checkRegister(@PathVariable("version") String version, @RequestParam("idClient") String id,
			@RequestParam("qos") int qos, @RequestParam("mqttBrokerURL") String mqttBrokerURL,
			@RequestParam("brokerUserName") String brokerUserName,
			@RequestParam("brokerPassword") String brokerPassword, @RequestParam("controllerId") String controllerId,
			@RequestParam("idUser") Integer idUser)
			throws JsonProcessingException, InterruptedException, Exception {
		return ResponseEntity.ok(globalMethods.Register(controllerId, version, mqttBrokerURL, id, brokerUserName, brokerPassword, qos,idUser).toByteArray());
	}

	@PostMapping("/{version}/Disconnect/{idClient}")
	public void Disconnect(@PathVariable("idClient") String idClient, @PathVariable("version") String version)
			throws MqttSecurityException, MqttException, org.eclipse.paho.client.mqttv3.MqttException {
		globalMethods.DisconnectAgent(idClient, version);	}

}
