package com.telnet.Mqtt.Services;



import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.protobuf.ByteString;
import com.telnet.Mqtt.modal.ConnectedClientv3;
@Service
public interface MqttMethodesV3 {
	public void publish(byte[] msg, String topic,String replyTopic, boolean retain, MqttClient mc, int qos)
			throws JsonProcessingException;

	public ByteString waitForResponse(String topic, MqttClient mqttClient, int qos, byte[] tr369Message)
			throws MqttException, InterruptedException, JsonProcessingException;

	public ConnectedClientv3 GetConnectedClient(String idClient);
	public void RemoveClient(String idClient) throws MqttException;
}	
