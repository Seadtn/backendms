package com.telnet.Mqtt.Clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(name = "auth-service", url = "${application.config.auth.url}")
public interface AuthClient {
	
	@PostMapping("/UpdateContactTime")
	public ResponseEntity<String> UpdateContactTime(@RequestParam String id);
	
	@PostMapping("/UpdateTime")
	public ResponseEntity<String> UpdateUpgradeTime(@RequestParam String id);
	
	
}
