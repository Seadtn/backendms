package com.telnet.Mqtt.modal;




import lombok.*;


@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
public class MqttAgent {
	private String idBoard;
    private String uspVersions;
    private String endpointId;
    private String protocol;
	private String brokerUrl;
    private String brokerUserName;
    private String brokerPassword;
    private String controllerId;
    private MQTTVersion mqttVersion;
    private int qos;
    private boolean tls;

    public enum MQTTVersion {
        v3_1_1,
        v3,
        v5;
    }
}
