package com.telnet.Authentication.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(name = "notification-service", url = "${application.config.notification.url}")
public interface PermissionClient {
	@PostMapping("/SendPermission")
	ResponseEntity<String> sendPermission(@RequestParam("idSender") Integer idSender,@RequestParam("idReciver") Integer idReciver,@RequestParam("username") String username,@RequestParam("idBoard") String boardId,@RequestParam("receiverName") String receiverName);
	@GetMapping("/getPermissionResp")
	ResponseEntity<String> getPermissionResp(@RequestParam("idSender") Integer idSender,@RequestParam("idReciver") Integer idReciver);
	@PostMapping("/deletePermission")
	ResponseEntity<String> deletePermission(@RequestParam("idSender") Integer idSender,@RequestParam("idReciver") Integer idReciver);
}
