package com.telnet.Mqtt.Clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@FeignClient(name = "history-service", url = "${application.config.history.url}")
public interface HistoryClient {
	@PostMapping("/save")
	public ResponseEntity<String> saveNewRequest(@RequestBody String requestJson);
}
