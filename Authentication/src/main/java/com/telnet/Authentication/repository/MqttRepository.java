package com.telnet.Authentication.repository;

import com.telnet.Authentication.modal.MqttAgent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MqttRepository  extends JpaRepository<MqttAgent, String> {
}
