package com.telnet.Authentication.modal;

public enum MQTTVersion {
    v3_1_1,
    v3,
    v5;
}
