package com.telnet.Mqtt.repository;

import com.google.gson.Gson;
import com.telnet.Mqtt.modal.ConnectedClientv3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MqttV3Repository {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private static final String KEY = "CLIENTV3";

    public void save(ConnectedClientv3 clientv3) {
        try {
            Gson gson = new Gson();
            String clientv3Json = gson.toJson(clientv3);
            redisTemplate.opsForHash().put(KEY, clientv3.getMqttAgent().getIdBoard(), clientv3Json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ConnectedClientv3 findById(String id) {
        try {
            Gson gson = new Gson();
            String clientv3Json = (String) redisTemplate.opsForHash().get(KEY, id);
            return gson.fromJson(clientv3Json, ConnectedClientv3.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(ConnectedClientv3 clientv3) {
        save(clientv3);
    }

    public void removeById(String id) {
        redisTemplate.opsForHash().delete(KEY, id);
    }
}
