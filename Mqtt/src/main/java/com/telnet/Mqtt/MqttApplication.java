package com.telnet.Mqtt;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableFeignClients
public class MqttApplication {
	public static void main(String[] args) {
		SpringApplication.run(MqttApplication.class, args);
	}

}
