package com.telnet.Authentication.modal;


import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.ManyToOne;
import lombok.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter @Setter
@ToString
public class Agent {
    @Id
    private String idBoard;
    private String uspVersions;
    private String endpointId;
    private String protocol;
    private String createdTime;
    private String lastUpdateTime;
    private String lastContactTime;
    private String connectedClients;
    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    private   Account account;
    @Enumerated(EnumType.STRING)
    private Status status;
}