package com.telnet.Mqtt.Services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttCallback;
import org.eclipse.paho.mqttv5.client.MqttClient;
import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.telnet.Mqtt.Clients.AuthClient;
import com.telnet.Mqtt.Clients.HistoryClient;
import com.telnet.Mqtt.modal.ConnectedClientv5;
import com.telnet.Mqtt.modal.History;
import com.telnet.Mqtt.repository.MqttV5Repository;

import lombok.RequiredArgsConstructor;
import usp.UspMsg13.Header.MsgType;
import usp.UspMsg13.Msg;
import usp_record.UspRecord13.Record;

@Service
@RequiredArgsConstructor
public class MqttV5MethodsImp implements MqttMethodesV5 {
	
	private final MqttV5Repository mqttV5Repository;
	private final HistoryClient client;
	private final AuthClient authClient;
	public static List<MqttClient> clientsv5= new ArrayList<MqttClient>();
	
	@Override
	public ConnectedClientv5 GetConnectedClient(String idClient) {
		MqttClient client=null;
		for (MqttClient mc : clientsv5) {
			if(mc.getClientId().equals(idClient)) {
				client=mc;
				break;
			}
		}
		ConnectedClientv5 clientv5= mqttV5Repository.findById(idClient);
		clientv5.setMqttClient(client);
		return  clientv5;
	}

	@Override
	public void publish(byte[] msg, String topic, String respTopic, boolean retain, MqttClient client, int qos)
			throws JsonProcessingException {
		try {
			MqttMessage message = new MqttMessage(msg);
			message.setQos(qos);
			message.setRetained(retain);
			MqttProperties properties = new MqttProperties();
			properties.setResponseTopic(respTopic);
			properties.setPayloadFormat(false);
			message.setProperties(properties);
			client.publish(topic, message);
			System.out.println("MQTTV5 : Published to " + topic);
		} catch (MqttException e) {

			e.printStackTrace();

		}

	}

	@Override
	public ByteString waitForResponse(String replyTopic, MqttClient mqttClient, int qos, byte[] tr369Message,Integer idUser,MsgType type,String RequestString)
			throws MqttException, InterruptedException, JsonProcessingException, InvalidProtocolBufferException {
		CountDownLatch responseReceivedLatch = new CountDownLatch(5);
		mqttClient.subscribe(replyTopic, qos);
	 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	 	Date date = new Date();  
		publish(tr369Message, "agent/" + mqttClient.getClientId(), replyTopic, true, mqttClient, 0);
		ByteString resp = addCallback(mqttClient, responseReceivedLatch);
		responseReceivedLatch.await(1, TimeUnit.SECONDS);
		History history =null;
		if(resp.isEmpty() || resp== null) {	
			 resp =ByteString.copyFromUtf8("No Response from the board");
			 history = new History(null, RequestString, new Gson().toJson(Msg.parseFrom(resp).getBody().getResponse()), "Failed :Time out", type.toString(), formatter.format(date), mqttClient.getClientId(), idUser);
		}else {
			 history = new History(null, RequestString,new Gson().toJson(Msg.parseFrom(resp).getBody().getResponse()), "Success", type.toString(), formatter.format(date), mqttClient.getClientId(), idUser);
		}
		client.saveNewRequest(new Gson().toJson(history));
		authClient.UpdateContactTime(mqttClient.getClientId());
		return resp;
	}

	private ByteString addCallback(MqttClient mqttClient, CountDownLatch responseReceivedLatch) {
		CompletableFuture<ByteString> future = new CompletableFuture<>();
		mqttClient.setCallback(new MqttCallback() {
			@Override
			public void messageArrived(String topic, MqttMessage message) throws Exception {
				Record record = Record.parseFrom(message.getPayload());
				future.complete(record.getNoSessionContext().getPayload());
				responseReceivedLatch.countDown();
			}

			@Override
			public void disconnected(MqttDisconnectResponse disconnectResponse) {
			}

			@Override
			public void mqttErrorOccurred(MqttException exception) {
			}

			@Override
			public void deliveryComplete(IMqttToken token) {
			}

			@Override
			public void connectComplete(boolean reconnect, String serverURI) {
			}

			@Override
			public void authPacketArrived(int reasonCode, MqttProperties properties) {
			}
		});
		try {
			return future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace(); 
			return null;
		}

	}

	@Override
	public void RemoveClient(String idClient) throws MqttException {
		ConnectedClientv5 clientv5= GetConnectedClient(idClient);
		if(clientv5.getNumber()>1) {
			clientv5.setNumber(clientv5.getNumber()-1);
			mqttV5Repository.update(clientv5);
		}else {
			clientv5.getMqttClient().disconnect();
			clientsv5.removeIf(client-> client.getClientId().equals(idClient));
			mqttV5Repository.removeById(idClient);
		}
	}

}