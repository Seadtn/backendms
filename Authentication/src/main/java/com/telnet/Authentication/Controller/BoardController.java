package com.telnet.Authentication.Controller;


import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.List;

import org.eclipse.paho.mqttv5.common.MqttException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.telnet.Authentication.authServices.authMethodsImp;
import com.telnet.Authentication.modal.Account;
import com.telnet.Authentication.modal.Agent;
import com.telnet.Authentication.repository.AccountRepository;
import com.telnet.Authentication.repository.AgentRepository;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;



@RestController
@RequestMapping("/auth/board")
@RequiredArgsConstructor
public class BoardController {
	
	private final   authMethodsImp auth;
	private final AgentRepository agentRepository;
	private final AccountRepository accountRepository;
    private final  KafkaTemplate<String, String> kafkaTemplate;

	@GetMapping("/getAllDevices")
	public ResponseEntity<?> getAllDevices(){
		 List<Agent> agents=agentRepository.findAll();
		 agents.forEach(a -> a.setAccount(null));
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(agents));
	}
	
	@GetMapping("/getDevicesByUser/{id}")
	public ResponseEntity<?> getUserDevices(@PathVariable("id") Integer num){
		 List<Agent> agents=agentRepository.findByAccountId(num);
		 agents.forEach(a -> a.setAccount(null));
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(agents));
	}
	@GetMapping("/getDevicesById/{id}")
	public ResponseEntity<?> getDeviceById(@PathVariable("id") String idboard){
		 Agent agent=agentRepository.findById(idboard).get();
		 agent.setAccount(null);
		return  ResponseEntity.status(HttpStatus.OK).body(new Gson().toJson(agent));
	}
	
	@PostMapping("/deleteDevice/{id}")
	public void deleteDevice(@PathVariable("id") String idboard){
		Agent agent=agentRepository.findById(idboard).get();
		Account account=agent.getAccount();
		List<Agent>agents= account.getListAgents();
		agents.remove(agent);
		account.setListAgents(agents);
		accountRepository.save(account);
		agentRepository.deleteById(idboard);
	}
	
	@PostMapping("/Register/{protocol}")
	public ResponseEntity<String> register(@PathVariable("protocol") String protocol,
			@RequestParam(name = "version", required = false) String version,
			@RequestParam(name = "idclient") String id, @RequestParam(name = "idcontroller") String controllerId,
			@RequestParam(name = "idAccount") Integer idAccount) throws Exception {
		Account account = accountRepository.findById(idAccount).get();
		return auth.CheckRegisterProtocol(protocol, version, id, controllerId,account);
	}

	@PostMapping("/connect")
	public ResponseEntity<String> connect(@RequestParam(name="idAccount") Integer idAccount,
			@RequestParam(name="idBoard") String idBoard) throws MqttException, JsonProcessingException, InterruptedException,
			org.eclipse.paho.client.mqttv3.MqttException {
		Account account = accountRepository.findById(idAccount).get();
		return auth.CheckConnectionProtocol(idBoard,account);
	}

	@PostMapping("/Disconnect/{id}")
	public ResponseEntity<String> disconnectAndRemoveClient(@PathVariable("id") String clientId,
			@RequestParam("protocol") String protocol,
			@RequestParam(name = "version", required = false) String version,
			@RequestParam(name="userName") String username) throws JsonProcessingException,
			MqttException, InterruptedException, org.eclipse.paho.client.mqttv3.MqttException {
		return auth.CheckDisconnectionProtocol(protocol, version, clientId,username);
	}
	
	@PostMapping("/UpdateContactTime")
	public ResponseEntity<String> UpdateContactTime(@RequestParam String id){
	    if(agentRepository.findById(id).isPresent()) {
	    Agent agent=agentRepository.findById(id).get();
	 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	 	Date date = new Date();  
	 	agent.setLastContactTime(formatter.format(date));
	 	agentRepository.save(agent);
	    return ResponseEntity.ok("Updated");
	    }
	    return ResponseEntity.ok("Error");

	}
	
	@PostMapping("/UpdateTime")
	public ResponseEntity<String> UpdateUpgradeTime(@RequestParam String id){
	    Agent agent=agentRepository.findById(id).get();
	 	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	 	Date date = new Date();  
	 	agent.setLastUpdateTime(formatter.format(date));
	 	agentRepository.save(agent);
	    return ResponseEntity.ok("Updated");
		
	}
    @Transactional
    public void transferAgent(String idBoard, Integer idNewOwner) {
        Agent agent = agentRepository.findById(idBoard).orElseThrow(() -> new RuntimeException("Agent not found"));
        Account oldAccount = agent.getAccount();
        if (oldAccount != null) {
            List<Agent> agents = oldAccount.getListAgents();
            agents.remove(agent);
            oldAccount.setListAgents(agents);
            accountRepository.save(oldAccount);
        }
        
        Account newAccount = accountRepository.findById(idNewOwner).orElseThrow(() -> new RuntimeException("Account not found"));
        List<Agent> newAgents = newAccount.getListAgents();
        newAgents.add(agent);
        newAccount.setListAgents(newAgents);
        agent.setConnectedClients("No Client is Connected");
        agent.setAccount(newAccount);
        
        agentRepository.save(agent);
        accountRepository.save(newAccount);
    }
    
    @Transactional
    @GetMapping("/ForceDisconnect")
    public void forceDisconnect(@RequestParam(name = "idBoard" ) String  idBoard,
			@RequestParam(name="connectedUser") String username,
			@RequestParam(name="idNewOwner" ,required = false) Integer idNewOwner
			) {
        Account acc = getAccountByUsername(username);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("idUser", acc.getId());
        jsonObject.addProperty("idBoard", idBoard);

        if (idNewOwner!=null) {
            transferAgent(idBoard, idNewOwner);
            jsonObject.addProperty("owner", "true");
        } else {
            jsonObject.addProperty("owner", "false");
        }

        kafkaTemplate.send("force-disconnect", new Gson().toJson(jsonObject));
    }

    public Account getAccountByUsername(String username) {
        if (username.contains("/Owner")) {
            return accountRepository.findByUsername(username.split("/")[0]).orElseThrow(() -> new RuntimeException("Account not found"));
        } else {
            return accountRepository.findByUsername(username).orElseThrow(() -> new RuntimeException("Account not found"));
        }
    }
}
